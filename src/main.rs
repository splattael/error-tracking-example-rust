use std::env;

fn main() {
    let sentry_dsn = env::var("ERROR_TRACKING_DSN").expect("Missing envvar ERROR_TRACKING_DSN");

    let _guard = sentry::init((
        sentry_dsn,
        sentry::ClientOptions {
            release: sentry::release_name!(),
            attach_stacktrace: true,
            send_default_pii: true,
            debug: true,
            ..Default::default()
        },
    ));

    // Works after merge of https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78590
    let err = "NaN".parse::<usize>().unwrap_err();
    sentry::capture_error(&err);

    // As of Jahttps://gitlab.com/gitlab-org/gitlab/-/issues/350669
    // Reason: events do not contain `exception` and are thus invalid
    sentry::capture_event(sentry::protocol::Event {
        message: Some("test event".into()),
        ..Default::default()
    });

    // As of January 2022, with gitlab 14.7.0-pre, this one does not work:
    // Reason: https://gitlab.com/gitlab-org/gitlab/-/issues/350416
    sentry::capture_message("test message", sentry::Level::Fatal);

    // This works!

    panic!("test panic");
}
