# Error Tracking Rust example

Based on https://gitlab.com/eminence/test-sentry-project.

## Testing

```bash
cp .env.sample .env
# Set ERROR_TRACKING_DSN

docker build -t sentry-rust .
docker run --rm -it --env-file .env sentry-rust

# Verify error showing up on the configured (ERROR_TRACKING_DSN) project
```

## Verify

![screenshot](/assets/screenshot.png)
