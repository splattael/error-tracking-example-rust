FROM rust:1

WORKDIR /usr/src/myapp

COPY Cargo.* ./
RUN mkdir src
RUN echo "fn main() {}" > src/main.rs
RUN cargo build
RUN rm -fr src target/debug/deps/test_sentry_project*

COPY . .

RUN cargo build

CMD ["cargo", "run"]
